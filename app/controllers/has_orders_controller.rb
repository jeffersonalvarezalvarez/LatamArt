class HasCartsController < ApplicationController

def create
	# Agregar al carro de compra

	@shopping_cart.in_shopping_cart.each do |i_sh|

		in_shopping_cart = HasOrder.new(listing: i_sh.listing,
									order_id: params[:listing_id])

		if in_shopping_cart.save
			redirect_to "/cart", notice: "Product saved to your cart"
		else
			redirect_to listings_path(id: params[:listing_id]), notice: "We can't save this product in your cart. Please try again"
		end

	end


end

def destroy
	# Eliminar el carro de compra
	@in_shopping_cart = InShoppingCart.find(params[:id])
	@in_shopping_cart.destroy
	
	redirect_to cart_path
end

end
