class ShoppingCartsController < ApplicationController
	
	def show
	end

	def update_quantity
		@in_shopping_cart = InShoppingCart.find(params[:id])
		@in_shopping_cart.update(:quantity => params[:quantity])
	end

end
