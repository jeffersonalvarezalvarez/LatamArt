class CommentsController < ApplicationController
  before_action :set_comment, only: [:update, :destroy]
  before_action :set_listing
  before_action :set_user

  # POST /comments
  # POST /comments.json
  def create

    @comment = current_user.comments.new(comment_params)

    if params[:listing_id]
      @comment.listing = @listing
    end

    if params[:user_id]
      @comment.user = @user
      @comment.user_name = current_user.first_name + ' ' + current_user.last_name 
      @comment.user_id_two = current_user.id
    end

    respond_to do |format|
      if @comment.save
        format.html { redirect_to :back, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { redirect_to user_profile_path(:nick => @user.nick), notice: @comment.errors }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to :back, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    
    def set_listing
      if params[:listing_id]
        @listing = Listing.find(params[:listing_id])
      end
    end

    def set_user
      if params[:user_id]
        @user = User.find(params[:user_id])
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:text, :qualification, :curator_id, :user_id, :listing_id)
    end
end
