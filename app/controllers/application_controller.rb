class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.  
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?
  protected

  def configure_permitted_parameters
    added_attrs_user_account_sign_up = [:first_name, :last_name, :photo, :type_user, :nick, :email, :terms_of_service]
    added_attrs_user_account_update = [:first_name, :last_name, :photo, :type_user, :nick, :email, :description, :telephone]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs_user_account_sign_up
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs_user_account_update
  end

  
  before_action :set_shopping_cart
  before_action :remove_elements_shopping

  private

    # Obtiene un carrito de la cookie si existe , y si no crea uno nuevo para el usuario
  	
    def set_shopping_cart
  		if cookies[:shopping_cart_id].blank?
  			@shopping_cart = ShoppingCart.create!(ip: request.remote_ip)
  			cookies[:shopping_cart_id] = @shopping_cart.id
  		else
  			@shopping_cart = ShoppingCart.find(cookies[:shopping_cart_id])
  		end

  		rescue ActiveRecord::RecordNotFound => e
  			@shopping_cart = ShoppingCart.create!(ip: request.remote_ip)
  			cookies[:shopping_cart_id] = @shopping_cart.id
  	end

    # Si hay objetos en el carrito de compra de productos mios, los elimina

    def remove_elements_shopping
      listings = Listing.where(user: current_user)
      @shopping_cart.in_shopping_cart.each do |i_sh|
        listings.each do |list|
          if i_sh.listing.id == list.id
            i_sh.destroy
          end
        end
      end
    end

end
