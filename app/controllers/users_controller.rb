class UsersController < ApplicationController
  before_filter :authenticate_admin!, only: [:index, :new, :create, :edit, :update, :destroy]

  def index

  	@users = User.all

  end

  def new

  	@user = User.new

  end

  def create

  	@user = User.new(user_params)
    @user.skip_confirmation!
    
  	respond_to do |format|
        if @user.save
          format.html { redirect_to users_path, notice: 'User was successfully created.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end

  end

  def edit

  	@user = User.find(params[:id])

  end

  def update

    @user = User.find(params[:id])

  	respond_to do |format|
        if @user.update(user_params_edit)
          format.html { redirect_to users_path, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit}
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end

  end

  def destroy

  	@user = User.find(params[:id])
  	if @user.destroy
  	  flash[:notice] = "Successfully deleted user."
  	  redirect_to root_path
  	end

  end 

  def show

  	@user = User.find_by_nick(params[:nick])
  	@comment = Comment.new

  end

  def listings
    @selected = Listing.where("user_id = ?", params[:user_id]).order(:name).paginate(:page => params[:page], :per_page => 6)

    respond_to do |format|
        format.js
    end
  end

  def comments
    @selected = Comment.where("user_id = ?", params[:user_id]).order(created_at: :desc).paginate(:page => params[:page], :per_page => 9)

    respond_to do |format|
        format.js
    end
  end

  private

    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :description, :telephone, :type_user, :password, :nick, :photo, :socialn1, :socialn2,:socialn3,:startPayment,:finishPayment,:statePayment)
    end

    def user_params_edit
      params.require(:user).permit(:first_name, :last_name, :email, :description, :telephone, :type_user, :nick, :photo, :socialn1, :socialn2,:socialn3,:startPayment,:finishPayment,:statePayment)
    end

end