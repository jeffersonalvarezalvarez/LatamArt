class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user
      sign_in @user, :event => :authentication #this will throw if @user is not activated
      
      if @user.type_user.blank?
        redirect_to edit_user_registration_path, notice: "With Facebook login you must update a few of your personal data"
      else
        redirect_to root_path, notice: "Logged in successfully with Facebook"
      end
        
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end

  end

  def failure
    redirect_to root_path
  end
end