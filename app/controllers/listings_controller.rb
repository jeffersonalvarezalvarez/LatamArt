class ListingsController < ApplicationController
  before_action :set_listing, only: [:show, :edit, :update, :destroy, :edit_admin, :update_admin, :destroy_admin]
  before_filter :authenticate_user!, only: [:seller, :new, :create, :edit, :update, :destroy]
  before_filter :authenticate_admin!, only: [:index_admin, :create_admin, :new_admin, :edit_admin, :update_admin, :destroy_admin]
  before_filter :check_user, only: [:edit, :update, :destroy]

  # Métodos del admin

  def index_admin

    @listings = Listing.all.order(:name)

  end


  def create_admin

    @listing = Listing.new(listing_params_admin)
    @listing.pictures << Picture.find(params[:photos].split(","))

    respond_to do |format|
      if @listing.save
        format.html { redirect_to artworks_list_path, notice: 'Listing was successfully created.' }
        format.json { render action: 'show', status: :created, location: @listing }
      else
        format.html { render action: 'new' }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end

  end

  def new_admin

    @listing = Listing.new

  end

  def edit_admin
  end

  def update_admin

    @listing.pictures << Picture.find(params[:photos].split(","))
    respond_to do |format|
      if @listing.update(listing_params_admin)
        format.html { redirect_to artworks_list_path, notice: 'Artwork was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end

  end

  def destroy_admin

    @listing.destroy
    respond_to do |format|
      format.html { redirect_to artworks_list_path, notice: 'Artwork was successfully deleted' }
      format.json { head :no_content }
    end

  end

  # Métodos normales

  def seller

    @listings = Listing.where(user: current_user).order("created_at DESC")

  end

  def index
   # @listings = Listing.select('"listings".*, (CASE WHEN "users"."startPayment" <= NOW() AND "users"."finishPayment" >= NOW() AND "users"."statePayment" = TRUE THEN \'1\' ELSE \'0\' END) AS priority').joins(:user).order('priority').group('"listings"."id"').paginate(:page => params[:page], :per_page => 14)
    @listings = Listing.all.order("random()").order('created_at DESC').paginate(:page => params[:page], :per_page => 8)   
    respond_to do |format|
      format.html
      format.js
    end
  end


  def subjects
    @selected = Subject.all.order("random()").paginate(:page => params[:page], :per_page => 6)

    respond_to do |format|
        format.js
    end
  end

  # Función para dividirlo en categorías
  def from_category

    if params[:cat_id] == "1"
      @selected = Listing.all.order("random()").paginate(:page => params[:page], :per_page => 8)
    else
      @selected = Listing.where(:category_id => params[:cat_id]).order(created_at: :desc).paginate(:page => params[:page], :per_page => 8)
    end 

    respond_to do |format|
        format.js
    end
  end



  

  # Función para dividirlo por precio
  def from_price
    minimium = Listing.all.minimum(:price)
    maximum = Listing.all.maximum(:price)
    range = ((maximum-minimium)/3).ceil

    if params[:range] == "range_price_1"
      @listings = Listing.where('price BETWEEN ? AND ?', minimium, minimium+range).paginate(:page => params[:page], :per_page => 12)
    elsif params[:range] == "range_price_2"
      @listings = Listing.where('price BETWEEN ? AND ?', minimium+range, minimium+(range*2)).paginate(:page => params[:page], :per_page => 12)
    elsif params[:range] == "range_price_3"
      @listings = Listing.where('price BETWEEN ? AND ?', minimium+(range*2), minimium+(range*3)).paginate(:page => params[:page], :per_page => 12)
    end 
  end 

  #Función para dividirlo por categorías
  def from_subject
    @listings = Listing.where('subject_id = ?', params[:subject]).order(:name).paginate(:page => params[:page], :per_page => 6)
  end

  #Función para la paginación de los comentarios
  def comments
    @selected = Comment.where("listing_id = ?", params[:listing_id]).order(created_at: :desc).paginate(:page => params[:page], :per_page => 9)

    respond_to do |format|
        format.js
    end

  end

  # GET /listings/1
  # GET /listings/1.json
  def show
    @comment = Comment.new
  end

  # GET /listings/new
  def new
    @listing = Listing.new
  end

  # GET /listings/1/edit
  def edit 
  end

  # POST /listings
  # POST /listings.json
  def create

    @listing = current_user.listings.new(listing_params)
    @listing.pictures << Picture.find(params[:photos].split(","))

    respond_to do |format|
      if @listing.save
        format.html { redirect_to @listing, notice: 'Listing was successfully created.' }
        format.json { render action: 'show', status: :created, location: @listing }
      else
        format.html { render action: 'new' }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update

    @listing.pictures << Picture.find(params[:photos].split(","))
    respond_to do |format|
      if @listing.update(listing_params)
        format.html { redirect_to @listing, notice: 'Listing was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing.destroy
    respond_to do |format|
      format.html { redirect_to listings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
      @listing = Listing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listing_params
      params.require(:listing).permit(:name, :description, :price, :quantity, :category_id, :subject_id, :currency_id)
    end

    def listing_params_admin
      params.require(:listing).permit(:name, :description, :price, :quantity, :category_id, :subject_id, :currency_id, :user_id)
    end

    def check_user
      if current_user != @listing.user
        redirect_to root_url, alert: "Sorry, this listing belongs to someone else"
      end
    end
end
