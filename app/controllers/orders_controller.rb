class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:index]
  before_action :authenticate_user!, only: [:new,:create, :sales, :purchases]

  def index
    @orders = Order.all
  end

  def sales
    @orders = Order.all.where(seller: current_user).order("created_at DESC")
  end

  def purchases
    @orders = Order.all.where(buyer: current_user).order("created_at DESC")
  end

  # GET /orders/new
  def new

    @order = Order.new

    if params[:listing_id]
      @listing = Listing.find(params[:listing_id])
    else params[:shopping_cart_id]
      @shopping = ShoppingCart.find(params[:shopping_cart_id])
    end

  end

  # POST /orders
  # POST /orders.json
  def create

    require 'paypal-sdk-adaptivepayments'
    PayPal::SDK.load('config/paypal.yml',  ENV['RACK_ENV'] || 'development')

    @api = PayPal::SDK::AdaptivePayments::API.new

    if params[:address_id]
      @address = Address.find(params[:address_id])
    else
      @address = Address.new(address_params)
      @address.user = current_user
      @address.save
    end

    if params[:listing_id]

      @order = Order.new
      @listing = Listing.find(params[:listing_id])

      @seller = @listing.user
      @order.listing_id = @listing.id
      @order.seller_id = @seller.id
      @order.buyer_id = current_user.id
      @order.address_id = @address.id
      @order.quantity = 1

      @pay_request = @api.build_pay()
      @pay_request.actionType   = "PAY"
      @pay_request.cancelUrl    = "http://localhost:3000/adaptive_payments/pay"
      @pay_request.currencyCode = "USD"
      @pay_request.feesPayer    = "SENDER"
      @pay_request.receiverList.receiver[0].amount = @listing.price
      @pay_request.receiverList.receiver[0].email  = @seller.email
      @pay_request.returnUrl    = "http://localhost:3000/adaptive_payments/pay"
      @pay_request.fundingConstraint.allowedFundingType.fundingTypeInfo = []
      @pay_request.sender.email = current_user.email

      @pay_response = @api.pay(@pay_request)

      if @pay_response.responseEnvelope.ack == 'Success'
        respond_to do |format|
          if @order.save

            @shopping_cart.destroy
            @listing.update(:quantity => (@listing.quantity - 1))

            format.html { redirect_to root_url, notice: @pay_response.responseEnvelope.ack }
            format.json { render action: 'show', status: :created, location: @order }
          else
            format.html { render action: 'new' }
            format.json { render json: @order.errors, status: :unprocessable_entity }
          end
        end
      else
        respond_to do |format|
            format.html { redirect_to root_url, notice: @pay_response.error.to_s }
            format.json { render action: 'show', status: :created, location: @order }
        end
      end 
    

    elsif params[:shopping_cart_id]

      @pay_request = @api.build_pay()
      @pay_request.actionType   = "PAY"
      @pay_request.cancelUrl    = "http://localhost:3000/adaptive_payments/pay"
      @pay_request.currencyCode = "USD"
      @pay_request.feesPayer    = "SENDER"

      items = @shopping_cart.in_shopping_cart.count
      @shopping = ShoppingCart.find(params[:shopping_cart_id])

      array = [];
      @shopping.in_shopping_cart.each do |i_sh|
        array << {i_sh.listing.user.email => i_sh.listing.price}
      end

      array = array.group_by{|h| h.keys.first}.values.map{|a| {a.first.keys.first => a.inject(0){|sum, h| sum + h.values.first.to_i}.to_s}}

      counter = 0
      array.each do |item|
        @pay_request.receiverList.receiver[counter].amount = item[item.keys[0]]
        @pay_request.receiverList.receiver[counter].email  = item.keys[0]
        counter = counter + 1
      end

      @pay_request.returnUrl    = "http://localhost:3000/adaptive_payments/pay"
      @pay_request.fundingConstraint.allowedFundingType.fundingTypeInfo = []
      @pay_request.sender.email = current_user.email

      @pay_response = @api.pay(@pay_request)

      if @pay_response.responseEnvelope.ack == 'Success'
        
        counter = 0
        suma = 0
        
        @shopping.in_shopping_cart.each do |i_sh|

          @order = Order.new
          @seller = i_sh.listing.user
          @order.listing_id = i_sh.listing.id
          @order.seller_id = @seller.id
          @order.buyer_id = current_user.id
          @order.address_id = @address.id
          @order.quantity = i_sh.quantity

          if @order.save
            i_sh.listing.update(:quantity => (i_sh.listing.quantity - i_sh.quantity))
            counter = counter + 1
            suma = suma + i_sh.listing.price
          end 
        
        end

        respond_to do |format|
          if counter == items
            @shopping_cart.destroy
            format.html { redirect_to root_url, notice: @pay_response.responseEnvelope.ack }
            format.json { render action: 'show', status: :created, location: @order }
          else
            format.html { render action: 'new' }
          end
        end

      else

        respond_to do |format|
          format.html { redirect_to root_url, notice: @pay_response.error.to_s }
          format.json { render action: 'show', status: :created, location: @order }
        end

      end

    end

  end

  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_list_path, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_order
      @order = Order.find(params[:id])
    end

    def address_params
      params.require(:address).permit(:address, :city, :country, :zip_code)
    end

end
