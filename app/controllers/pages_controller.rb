class PagesController < ApplicationController

  def about
  end
   def exhibition
     
   end

  def Appreciation
    @comments= Comment.all.order("random()").paginate(:page => params[:page], :per_page =>6)
  end
   

  def artist
 @results = User.where("CAST(type_user AS text) = '1'").paginate(:page => params[:page], :per_page => 6)
    respond_to do |format|
      format.html
      format.js

    end
  end
   

   def styles
    @categories = Category.all.order(:name)
    @listings = Listing.all.order("random()").paginate(:page => params[:page], :per_page =>6)
    @subjects = Subject.all.order("random()").paginate(:page => params[:page], :per_page =>6)
   respond_to do |format|
      format.html
      format.js

    end
  end
  
  def subjects
    @selected = Subject.all.order("random()").paginate(:page => params[:page], :per_page => 6)

    respond_to do |format|
        format.js
    end
  end


    def from_subject
    @listings = Listing.where('subject_id = ?', params[:subject]).order(:name).paginate(:page => params[:page], :per_page => 6)
  end


  def contact
  end

  def elearning
  end

  def pages_category    
    @subjects = Subject.joins("INNER JOIN listings ON subjects.id = listings.subject_id INNER JOIN categories ON categories.id = listings.category_id").where("categories.name LIKE ?", 
      params[:category].capitalize).group('id').paginate(:page => params[:page], :per_page => 6)
   
    @listings = Listing.joins(:category).where("categories.name LIKE ?", 
      params[:category].capitalize).paginate(:page => params[:page], :per_page => 6)
     respond_to do |format|
      format.html
      format.js

    end
  end
     
  

  def pages_subject    
    @listings = Listing.joins("INNER JOIN subjects ON subjects.id = listings.subject_id
          INNER JOIN categories ON categories.id = listings.category_id").where('subjects.name LIKE ?', params[:subject].capitalize).paginate(:page => params[:page], :per_page => 6)
  respond_to do |format|
      format.html
      format.js

    end
  end

 def search
    palabra = "%#{params[:keyword]}%"
    @results = Listing.where("name ILIKE ?", palabra) + User.where("CAST(type_user AS text) = '1' AND (first_name ILIKE ? OR last_name ILIKE ? OR nick LIKE ?)", palabra, palabra, palabra) + Subject.where("name ILIKE ?",palabra)
  end
end
