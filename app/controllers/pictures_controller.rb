class PicturesController < ApplicationController
before_action :set_picture, only: [:destroy]

  def create
    ActiveRecord::Base.transaction do
      params[:images].each do |image|
        @picture = Picture.create!(image: image)
        if @picture.save
          respond_to do |format|
            format.html { render json: @picture.to_jq_upload, content_type: 'text/html', layout: false }
            format.json { render json: @picture.to_jq_upload }
          end
        else
          render json: { error: @picture.errors.full_messages }, status: 304
        end
      end
    end
  end

  def update
    ActiveRecord::Base.transaction do
      params[:images].each do |image|
        @picture = Picture.create!(image: image)
        if @picture.save
          respond_to do |format|
            format.html { render json: @picture.to_jq_upload, content_type: 'text/html', layout: false }
            format.json { render json: @picture.to_jq_upload }
          end
        else
          render json: { error: @picture.errors.full_messages }, status: 304
        end
      end
    end
  end

  def destroy
    @picture.destroy
    head :no_content
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_picture
      @picture = Picture.find(params[:id])
    end

end
