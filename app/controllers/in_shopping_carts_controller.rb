class InShoppingCartsController < ApplicationController

def create
	# Agregar al carro de compra
	if !(InShoppingCart.joins("INNER JOIN listings ON listings.id = in_shopping_carts.listing_id
          INNER JOIN shopping_carts ON shopping_carts.id = in_shopping_carts.shopping_cart_id").where("listings.id = ? AND shopping_carts.id = ? ", params[:listing_id], @shopping_cart.id).present?)
		
		in_shopping_cart = InShoppingCart.new(listing_id: params[:listing_id],
								shopping_cart: @shopping_cart)
	
		if in_shopping_cart.save
			redirect_to "/cart", notice: "Product saved to your cart"
		else
			redirect_to listings_path(id: params[:listing_id]), notice: "We can't save this product in your cart. Please try again"
		end

	else 

		redirect_to "/cart", notice: "Product saved to your cart"
		
	end

end

def destroy
	# Eliminar el carro de compra
	@in_shopping_cart = InShoppingCart.find(params[:id])
	@in_shopping_cart.destroy
	
	redirect_to cart_path
end

end
