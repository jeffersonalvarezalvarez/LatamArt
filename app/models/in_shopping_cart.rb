class InShoppingCart < ApplicationRecord
  belongs_to :listing
  belongs_to :shopping_cart
  has_one :user, through: :listing
end
