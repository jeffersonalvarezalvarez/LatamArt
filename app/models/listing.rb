class Listing < ActiveRecord::Base

  validates :name, :description, presence: true
  

  belongs_to :user
  belongs_to :category
  belongs_to :subject

  has_many :comments, :dependent => :destroy
  has_many :pictures, :dependent => :destroy
  has_many :orders, :dependent => :destroy

end
