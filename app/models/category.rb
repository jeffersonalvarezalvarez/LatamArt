class Category < ApplicationRecord

  mount_uploader :photo, PictureCategoryUploader

  validates :name, presence: true
  validates :description, presence: true
  has_many :listings

end
