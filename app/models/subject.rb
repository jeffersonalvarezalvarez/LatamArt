class Subject < ApplicationRecord
	has_many :listings
	mount_uploader :image, PictureArtUploader
end
