class BankAccount < ActiveRecord::Base

  validates :bank, presence: true
  validates :number, presence: true
  validates :country, presence: true

  belongs_to :user

end