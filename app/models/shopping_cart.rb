class ShoppingCart < ApplicationRecord
	has_many :listings, through: :in_shopping_cart
	has_many :in_shopping_cart, dependent: :destroy

	enum status: {payed: 1, default: 0}
end
