class ApplicationMailer < ActionMailer::Base
  default from: 'soporte@elpost.co'
  layout 'mailer'
end

