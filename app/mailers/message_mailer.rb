class MessageMailer < ApplicationMailer
	default from: "Noreply <soporte@elpost.co>"
	
  	def new_message(message)
    	@message = message
   	 	mail subject: "Message from #{message.name}"
  	end

end
