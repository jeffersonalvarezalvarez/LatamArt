Rails.application.routes.draw do

  resources :posts
  resources :addresses, only: [:create, :destroy, :update, :edit, :new]
  resources :bank_accounts, only: [:create, :destroy, :update, :edit, :new]
  resources :pictures
  resources :in_shopping_carts, only: [:create, :destroy]
  resources :orders, only: [:new, :create, :destroy]

  devise_for :admins, path_names: {
    sign_up: ''
  }

  devise_for :users, controllers: { registrations: 'users/registrations', passwords: "users/passwords", :omniauth_callbacks => "users/omniauth_callbacks" }

  authenticated :admin do
    root to: 'users#index'
  end

  authenticated :user do
    root to: 'listings#index'
  end
  
  resources :listings do
    resources :orders, only: [:new, :create]
    resources :comments, only: [:new, :create, :destroy]
  end

  resources :shopping_carts, only: [:show] do
    resources :orders, only: [:new, :create]
  end

  resources :users do
    resources :comments, only: [:new, :create, :destroy]
  end

  # Usuarios admin  
  get "admins/users/new" => 'users#new', as: 'add_user_admin'
  post "admins/users/new" => 'users#create', as: 'login_admin_user'
  get "admins/users/:id/edit" => "users#edit", as: "edit_user_adm"
  patch "admins/users/:id" => 'users#update', as: 'edit_user_admin'
  delete "admins/users/:id" => 'users#destroy', as: 'destroy_user_admin'
  get "admins/users" => 'users#index', as: 'list_user_admin'

  # Moneda admin
  get "admins/currencies" => 'currencies#index', as: 'currencies'
  post "admins/currencies" =>  'currencies#create'
  get "admins/currencies/new" => "currencies#new", as:'new_currency'                             
  get "admins/currencies/:id/edit" => "currencies#edit", as: "edit_currency" 
  get "admins/currencies/:id" => "currencies#show", as:"currency"
  patch "admins/currencies/:id" => "currencies#update"
  put "admins/currencies/:id" => "currencies#update"
  delete "admins/currencies/:id" => "currencies#destroy"

  #subjects admin
  get "admins/subjects" => 'subjects#index', as: 'subjects'
  post "admins/subjects" =>  'subjects#create'
  get "admins/subjects/new" => "subjects#new", as:'new_subject'                             
  get "admins/subjects/:id/edit" => "subjects#edit", as: "edit_subject" 
  get "admins/subjects/:id" => "subjects#show", as:"subject"
  patch "admins/subjects/:id" => "subjects#update"
  put "admins/subjects/:id" => "subjects#update"
  delete "admins/subjects/:id" => "subjects#destroy"

  #categorias admin
  get "admins/categories" => 'categories#index', as: 'categories'
  post "admins/categories" =>  'categories#create'
  get "admins/categories/new" => "categories#new", as:'new_category'                             
  get "admins/categories/:id/edit" => "categories#edit", as: "edit_category" 
  get "admins/categories/:id" => "categories#show", as:"category"
  patch "admins/categories/:id" => "categories#update"
  put "admins/categories/:id" => "categories#update"
  delete "admins/categories/:id" => "categories#destroy"

  #order admin
  get "admins/orders" => 'orders#index', as: 'orders_list'
  delete "admins/orders/:id" => "orders#destroy"

  #artwork admin
  get "admins/artworks" => 'listings#index_admin', as: 'artworks_list'
  post "admins/artworks/new" =>  'listings#create_admin'
  get "admins/artworks/new" => "listings#new_admin", as:'new_listing_admin'                             
  get "admins/artworks/:id/edit" => "listings#edit_admin", as: "edit_listing_admin" 
  patch "admins/artworks/:id" => "listings#update_admin", as: "update_listing_admin"
  put "admins/artworks/:id" => "listings#update_admin"
  delete "admins/artworks/:id" => "listings#destroy_admin", as:"destroy_listing_admin"

  #addresses admin
  get "admins/addresses" => 'addresses#index_admin', as: 'addresses_list'
  post "admins/addresses/new" =>  'addresses#create_admin', as:'create_address_admin'
  get "admins/addresses/new" => "addresses#new_admin", as:'new_address_admin'                             
  get "admins/addresses/:id/edit" => "addresses#edit_admin", as: "edit_address_admin" 
  patch "admins/addresses/:id" => "addresses#update_admin", as: "update_address_admin"
  put "admins/addresses/:id" => "addresses#update_admin"
  delete "admins/addresses/:id" => "addresses#destroy_admin", as:"destroy_address_admin"

  #bank accounts_admin
  get "admins/bank_accounts" => 'bank_accounts#index_admin', as: 'bank_accounts_list'
  post "admins/bank_accounts/new" =>  'bank_accounts#create_admin', as: 'create_bank_account_admin'
  get "admins/bank_accounts/new" => "bank_accounts#new_admin", as:'new_bank_account_admin'                             
  get "admins/bank_accounts/:id/edit" => "bank_accounts#edit_admin", as: "edit_bank_account_admin" 
  patch "admins/bank_accounts/:id" => "bank_accounts#update_admin", as: "update_bank_account_admin"
  put "admins/bank_accounts/:id" => "bank_accounts#update_admin"
  delete "admins/bank_accounts/:id" => "bank_accounts#destroy_admin", as:"destroy_bank_account_admin"

  #AJAX
  get "/fetch_listings" => 'listings#from_category', as: 'fetch_listings'
  get "/fetch_subjects" => 'listings#subjects', as: 'fetch_subjects'
  get "/fetch_comments_listing" => 'listings#comments', as: 'fetch_comments_listing'
  get "/fetch_listings_user" => 'users#listings', as: 'fetch_listings_user'
  get "/fetch_comments_user" => 'users#comments', as: 'fetch_comments_user'

  get 'pages/styles'
  get 'pages/Appreciation'
  get 'pages/exhibition'
  get 'pages/artist'
  get 'pages/museums'

  get "cart", to: "shopping_carts#show"
  get "/add/:listing_id", as: :add_to_cart, to: "in_shopping_carts#create"
  get "price/:range" => 'listings#from_price', as:'range_price'
  get "pages_category/:category/" => 'pages#pages_category', as: 'pages_category'
  get "pages_subject/:subject/" => 'pages#pages_subject', as: 'pages_subject'
  get "bysubject/:subject" => 'listings#from_subject', as: 'from_subject'
  post "cart/update_quantity" => "shopping_carts#update_quantity", as:'update_quantity'
  get "/search" => "pages#search", as:"search"
  get 'contact', to: 'messages#new', as: 'pages_contact'

  post 'contact', to: 'messages#create'
  get '/listings' => 'listings#index', defaults: { format: 'js' }
  get "/elearning" => 'pages#elearning', as:"pages_elearning"
  get "about" => 'pages#about', as:"pages_about"
  get 'seller' => "listings#seller"
  get 'sales' => "orders#sales"
  get 'purchases' => "orders#purchases"
  get '/:nick' => 'users#show', as:"user_profile"

  root 'listings#index'
  
end
