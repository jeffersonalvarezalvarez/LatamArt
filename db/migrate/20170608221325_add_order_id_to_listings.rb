class AddOrderIdToListings < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :category_id, :integer
    add_index  :listings, :category_id
  end
end