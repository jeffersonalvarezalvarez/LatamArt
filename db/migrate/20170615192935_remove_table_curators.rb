class RemoveTableCurators < ActiveRecord::Migration[5.0]
  def change
  	drop_table :curators, force: :cascade
  end
end
