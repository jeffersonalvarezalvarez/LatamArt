class CreateBankAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :bank_accounts do |t|
      t.string :bank
      t.string :number
      t.string :country

      t.timestamps
    end
  end
end
