class AddImageToSubject < ActiveRecord::Migration[5.0]
  def change
  	add_column :subjects, :image, :string
  end
end
