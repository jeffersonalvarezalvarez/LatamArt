class RemoveColumnsOrders < ActiveRecord::Migration[5.0]
  def change

  	remove_column :orders, :address, :string
  	remove_column :orders, :city, :string
  	remove_column :orders, :state, :string

  	add_column :orders, :address_id, :integer

  end
end
