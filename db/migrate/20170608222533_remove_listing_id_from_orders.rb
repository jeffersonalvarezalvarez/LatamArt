class RemoveListingIdFromOrders < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :listing_id, :integer
  end
end
