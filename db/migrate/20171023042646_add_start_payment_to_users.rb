class AddStartPaymentToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :startPayment, :date
  end
end
