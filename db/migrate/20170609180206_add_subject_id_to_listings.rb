class AddSubjectIdToListings < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :subject_id, :integer
  end
end
