class CreateHasOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :has_orders do |t|
      t.integer :status
      t.references :listing, foreign_key: true
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
