# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Admin
Admin.create!({:email => "admin@latamart.com", :password => "qwerty12345", :password_confirmation => "qwerty12345" })

user = User.new(
    :first_name => 'Ileana',
    :last_name => 'Gutierrez',
    :description => "“New paintings”
Ileana Gutierrez is one of most impressive representatives of the contemporary Colombian fine arts. Her art works are hardly possible to definitely attribute to some particular style or trend. The style of her paintings is always individual and recognizable though every time it remains within the framework of the figurative paintings.
One of the great influences has been France , she attended Le Ecole des Beaux Arts in Paris.
Ileana Gutierrez´s landscapes most often communicate her longing for the sea. We are fascinated by the quiet sea, and the silhouettes of the boats lingering in the day light.
In this recent body of work Ileana handles the texture, color and form of the landscape with a sculpting hand, almost carving the paint on to the canvas with a layering of color and varied brushwork. . These pictures are explorations of the cutting and layering of shape and color in a sweeping landscape and they maintain a delightful equilibrium between representational landscapes and landscape as abstract inspiration.
She constantly experiments with techniques and materials - finding new ways to reflect the changing light and seasons in her work.
Ileana never allows the sentiment behind a work , she uses the color, tone and mood, giving the eye a chance to relax and imagine , what she calls “sentimentismo”. They represent a consensus, derived from experience, about the common sentiments of mankind. Those finer emotions between the form and the sentiment.",
    :telephone => '+57 3163923267',
    :email => 'ileana@gmail.com',
    :password => 'password6',
    :password_confirmation => 'password6',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Javier',
    :last_name => 'Santamaria',
    :description => "Javier Santamaría is considered a representative artist of the neofuturism, juxtaposed with geometric forms. These elements, which initially appear disconnected, reveal on closer inspection a strong link. Santamaría use of multi-pictorialism in which images are loaded with information and cross reference, instigates the viewer to find hidden meanings and layers.
Paintings seek to reflect both life and his work as an act of passage, his use of color creates a powerful though intimate presence. Figures possess the frictionless purity of an unreal and recurring dream, in which every figure is memorable without ever possessing any individual traits. The form is softer, but the critical impact is no less strong.
The resulting visual impact is highly poetic and often suggests the cascading lines of a musical score, forms are perceptually well defined. Viewers can appreciate the artist’s ease at creating surprising compositions that are the result of a studied and precise balance, nothing is improvised. His works are in the hands of art collectors in U.S.A., Canada, England, Germany, Spain, Colombia and currently exhibits in art galleries in different cities.",
    :telephone => '+57 3007656638',
    :email => 'correo@santamariaart.com',
    :password => 'password8',
    :password_confirmation => 'password8',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Pablo',
    :last_name => 'Zapata',
    :description => "In the abstract images of Colombian artist Pablo Zapata, visions of color and movement converge to create unexpected configurations of line and form. A complexity inherent in the manner in which everything comes together, creating an extraordinary synchronicity that speaks of a depth of meaning and purpose in each of his works.
In Zapata´s art anyone can connect regardless of background or intent. For him, the representational aspect of painting is less important than his interpretation of the reality of the emotive force the thing contains . The simultaneous combined effect of interacting elements in his paintings besiege the viewer with feelings. Expressing not only space itself but also the timeless play of space and energy, space continually gives birth to energy and energy dissolves into space , energy sparkles as a few small diversified shapes in strikingly bright colors. His art uses shape, color and, later, texture and the movement of paint to evoke a world of emotion and imagination.
By -2011s he had forged a distinctive personal style which advanced a startling use of simple shapes and high-key color. These paintings defied the modernist insistence on a flat picture surface. Instead they emphasized the illusion of space.His art has been featured in several art galleries in the Unites States and Colombia",
    :telephone => '+57 3007656638',
    :email => 'pazartepz@hotmail.com',
    :password => 'password9',
    :password_confirmation => 'password9',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Angela',
    :last_name => 'Castro',
    :description => "Casting Brilliance by Angela Castro
Angela Castro is a glass artist from Bogotá, Colombia who has worked with fused glass art for more than 20 years. Warm glass is art glass that is formed in a kiln. This process involves firing a mosaic of compatible glass in a kiln up to 1500 degrees until it fuses together. The alchemy of intense heat creates the illusion of fluidity in each unique piece. Castro's sculpture has an extraordinary ability to make a first-time viewer stop and be drawn in to look carefully again and again. Her brilliance has given her the cutting edge in kiln-cast glass for her inspirational work. By using glass in innovative ways there are optical illusions within the pieces that you see With casts taken from familiar objects inside, for example fruit and vegetables, with opaque and reflective glass become both majestic and magical.
Because glass is liquid even in its (seemingly) solid state, it has unusual versatility and shows magical qualities as the colors change with light. Light filtering through transparent or translucent glass plays with our perceptions and gives a sense of depth. The reflective qualities of iridescent and dichroic glass have a brilliance that changes with each viewing position.
Angela finds that the properties of transparency, refraction, and magnification set glass apart from other art mediums. The optics of glass can bring the illusion of movement and life to a sculpture. When the alchemy of intense heat is added, the fluid nature of fused glass holds a fascination that no other medium can satis provide.
Angela’s current work flows from realistic representations of nature to abstract interpretations of the essence and palette of her environment.
Part of her work is done in her own transparent scenario with a special code or “DNA” including silica, calcium carbonate, mineral oxides and metals.-",
    :email => 'vitrafenix@hotmail.com',
    :password => 'password10',
    :password_confirmation => 'password10',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Maria Isabel',
    :last_name => 'Salazar de Lince',
    :description => "Maria Isabel abstract landscapes in 'Sensations', evoke the world's constant flux and passages of rare beauty.with a balanced composition and the right kind of texture and tone.
Maria Isabel- is a passionate artist using an innovative technique in oil paint that captures emotion and awakens the senses… She modifies  the observed subject by introducing a subjective element of her own, the 'feeling of being there' is apparent. Each work contains a pole of stillness and a pole of violence; from both of which a remarkable aesthetic unity evolves. The only recurring motif found is a playful smuggling of unexpected blues into  monochrome skies and seas.
Her current artwork, with an abstract spirit combined with figurative references, features strength through color and freedom evidenced in shapes.. This is earthy, grounded, sensible art at its peak of accomplishment and maturity.
She has been invited  to National Exhibition of Fine Arts - Carrousel du Louvre , Paris.  Every year, the National Exhibition of Fine Arts presents paintings, sculptures and prints in the triangle of the Louvre Pyramid. ",
    :email => 'midelince@gmail.com',
    :password => 'password11',
    :password_confirmation => 'password11',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Maria Del Pilar',
    :last_name => 'Granados',
    :description => "Approaching painting with a sensitivity and attentiveness peculiar to women, Pilar Granados makes incredible paintings depicting the true beauty of nature with an excellent blend of colors. The colors with her observation and expertise bring her paintings to life. She has found a provocative correspondence between the abstract dynamism in a void and the graphic features of nature. Her bold strokes and fine outlines support a unified field, where modern discrete tones that make a surprising aspect of space.
Her works are characterized by vivid yet calm colors, which are unique in her paintings. Ultimately, each piece captures some fragment of the landscape´s power until the large body of the work joins to express its deepest essence. She has been in many exhibits in Colombia, France, Germany, Chile, and Cuba.
Pilar Granados has been painting and drawing for thirty years. She studied, painting and engraving in Vienne at Atelier Maller Osorio and then in Suisse, Holland, Belgium and Czechoslovakia. She taught seminars in painting and drawing for “La Contraloria General de la Republica” and also works at “Le monde Diplomatique.” Since 2005 belongs to the Bogota Art Foundation, and sets its exchange with artists from Cuba, especially with Santiago in the annual Arts Festival.",
    :telephone => '+57 3007427094',
    :email => 'pigraji@yahoo.com',
    :password => 'password12',
    :password_confirmation => 'password12',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Diana Patricia',
    :last_name => 'Soto Aristizabal',
    :description => "Abstract Drawings by Diana Patricia Soto Aristizabal
Colombian born Diana Patricia graduated with a distinction in the Technological University, Pereira (Higher National Diploma in music ) and from the University Jorge Tadeo Lozano Fine Arts. Her drawings with a highly finished compositions with rich possibilities of abstraction as a mode of artistic expression.The only lines visible are those to portray a texture in her subjects, relying on the light falling across the surface. It is the shadows created by the light that reveal the textures of the subject., the imaginary space in front and wandering off into its marks and empty passages. Creatings works of partial or complete abstraction exclusive or in addition to paintings for the unique properties and effects afforded to them by drawing media such as charcoal and acrylic.
Her clean colours and strict mathematical constructions acting as a structure out of which gesture explodes out of the space creating a dynamic edge, a feeling that the technical restrictions of her self imposed design .Like all work, it’s more about movement than anything else, phenomenally speaking.
The magnetism of the painted surface invites exploration, the feel of the paint and space stimulates directness void of storytelling. It is as you see it. At times the paintings become overworked, making it a challenge to end up with a fresh statement, where the painting has precision, luminosity, clarity of inner light, air and colour.
Patricia´s drawings and paintings present harmony, chaos and a sense of momentum. Her work has been included in exhibitions across the country in France in the Carrusel du Louvre, Latino Art Museum,and Pomona California.For two years she had the first prize. “Rotary in Art”. Mural for Peace, 2014 organized by Américas Museum.. Her work has been purchased by collectors internationally.",
    :email => 'dipasoar@hotmail.com',
    :password => 'password15',
    :password_confirmation => 'password15',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Lucia ',
    :last_name => 'Nebel',
    :description => "Lucia Nebel “ANCESTRAL SILENCE” latam series.
“A nostalgic look of a creative moment in a past present, with their rituals, their ancestral forms that recall praying, giving the gods their subject of worship”.
“I seek balance with nature, today I find myself in those ancestors who have come to me as a sacred element. Way with them, their suns, moons, its high mystical forms, myths and legends, thus achieving a connection with the past. I look back and I see in the oxidation time, the silent path of oblivion”: Lucia Nebel
Lucia´s composition and the warm patinas, volume and space are beautiful. The various shapes in the series give the iron and  steel sculpture a magnificent atmosphere.
Her need to reach a synthesis leads her to abstract gestural compositions with pre-Columbian reminiscences and a permanent questioning about where do we come from and who  we are. She believes in art that has content and meaning with a latam slant.
Her work has been exhibited in many countries including Colombia, Ecuador, USA, China, Japan and Italy. She had won several deserved awards and honors",
    :email => 'lunebel@hotmail.com',
    :password => 'password16',
    :password_confirmation => 'password16',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Pilar',
    :last_name => 'Mejia',
    :description => "“Barranquilla-based Henry Alviar is known for his investigations into memory, nature and the construction of animals through butterfly sculptures.
Alviar’s sculptures, so responsive to transformations in the light and air surrounding them, are about fluid movement, color and change, reminding us that everything is in flux. Though perfectly realistic in appearance these constructions manage to highlight the rhythms of nature, they create evanescent drawings in thin air, collaborating with the landscape.
 
The ambitious and imaginative structure of Alviar’s sculpture offers an uncanny visual metaphor for Garcia Marquez’s which is often considered the ultimate Latin American novel and considered the father of the literary genre known as magical, constantly represented by yellow butterflies. For the yellow butterflies fluttering around Mauricio Babilonia in One Hundred Years of Solitude. Alviar creates large scale sculptures and intimate, interior pieces that are so strong in any size.
Creating an effect of mystery and wonder, a strong source of idea, masterfully realized, movement and energy through the dynamism, seeks the balance of composition. His sculptures offer aerial movements, round shapes with powerful lines, delicate and sensual cuts. The contextualization of his works, inscribing the approach of Henry Alviar through field of architecture. The beautiful subject appeals universally to a broad audiences of collectors.",
    :email => 'pilarmejia@me.com',
    :password => 'password25',
    :password_confirmation => 'password25',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Juan',
    :last_name => 'Acevedo',
    :description => "“In Juan Mario´s works the physical and gestural aspects of the artistic process are emphasized along with his ability to create truly beautiful paintings in a confident, mature style. The way he lays colors and the stateliness of his compositions maintain a fine balance between seemingly contradictory forces: control, spontaneity and imagery .
The abstract expressionism paintings and sculptures from the past year, offer a critical reading of abstraction itself Acevedo´s eccentrically shaped or multi-part canvases, loosely painted, executed with excruciating care interlaces with trend. The colors, the textures, the tension and the fragments, brilliantly conjugate with the contemporary. Acevedo´s current work within many of his paintings elevates the written word to subject and transforms traditional viewing into a type of reading. From the abstract to the postmodern world this painter´s real contribution to contemporary art is that which painting always tended to reveal in depth of meaning over time. 

His gestural language allows a reading of the ego as an event that goes beyond any rational attitude. The artist chooses the place to transform the plastic phenomenon into a ritual in an opening action, allowing the vital energy to flow in a moving gesture that becomes visible in his pictorical writing, through which he reflects the state of the soul of our time. He becomes related to the construction of trans vanguard movements, strongly saturated colors slide over the canvas to give shape to an amazing context. The support creates atmospheres in which figure and background interpenetrate in corresponding dialogues. The lively mind and entrepreneurial spirit of Acevedo transmits happenings and environments, which redirect the focus from the artist as actor to the audience as creators. A wide variety of curving and flowing linear elements the shapes and direct the viewer´s eyes over the complex composition in continuous sometimes sharp motions.",
    :email => 'jmart@juanmarioacevedo.com',
    :password => 'password26',
    :password_confirmation => 'password26',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Fernando',
    :last_name => 'Pinto',
    :description => "“Fernando Pinto, is a skill abstract minimalist colombian sculptor. He masters the art of marble,white quartzite stone and granite bringing to life rough, dense materials and its monolithic blocks, liberating living forms from within these materials . Central theme of his work is the contemplative human being, as an abstract figure, reflecting on the longing for harmony and communication. An abstract sculptor, Pinto explores both the complexity of the non-representational and the expansiveness created by working on non-traditional supports.
If ‘line’ is the main protagonist in most of his works then space, rhythm and light is its context. This is about perception, emotional responses and thinking in the “here and now”. The surfaces, which seem to absorb natural daylight in order for it then to be emanated, have a strange diaphanous or luminous effect. The small format series, reflecting on conflicts between human beings. In the end these reflections that are derived from civic conflicts in Colombia, but at the end are simply human conflicts, evidence of contrasts generated in a civilized society. His works are conceptual in theme, emphasizing varieties of line, texture, color, form and shadow; sense of movement is captured and translated into his marble or stone linear sculptures.
Fernando Pinto selected in Taiwan by the International Cultural Council. Winner of the 2012 International Chaco Sculpture Biennial, the Guadalajara Sculpture Biennial in Mexicoand Bienal del Chaco, Argentina. He has exhibited extensively in Spain, Belgium, France, Italy, Japan and Australia. Also in Peru, Italy , Japan, Mexico, Syria, Colombia, South Korea, India, Portugal, Argentina, Spain, Leban and Taiwan.
Prizes;1st prize 14º Simposio Internazionale Nantopietra, Vicenza, Italia. 2do prize II Bienal de Escultura de Guadalajara, Guadalajara, México.1st grand prize, Bienal del Chaco, Resistencia.",
    :email => 'sculpinto@yahoo.com',
    :password => 'password28',
    :password_confirmation => 'password28',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Alberto',
    :last_name => 'Anzola',
    :description => " “Alberto Anzola is essentially Latam due to his Colombian origin.  In the present phase of his production, he has matured a new artistic language, proper, very personal, enriched by a coherent formal vocabulary and a thematic repertoire that centers on the human figure. It´s a non-realistic figure, not intended to describe its visual appearance or to serve in analytic speculation. Contemporary with an emphasis on abstract and figurative  forms with organic lines and  form. Alberto Anzola has been able to develop various techniques such as casting lost, casting and modeling wax by using new materials such as iron, steel and polymers.
Over the last few years he has been exploring the bond between bulls. His sculptures capture an element of tension, humour and sensitivity, leaving one with the impression that there is often more than meets the eye,  presenting abstracted human forms in the most aesthetically pleasing and harmonious way, playing with the negative spaces as well as the shape itself. With his organic sculptures he has established a position on the latam art scene.
The relationship between the internal and external realities; the dualism between inside and outside, content and form, feeling and shape, impression and expression. The shape consists of continuously flowing inner and outer surfaces, with one line running through the form. The capacity for synthesis that Anzola asserts in his pieces is one of the most noticeable and persistent characteristics of his work.",
    :email => 'escultoresanzola@gmail.com',
    :password => 'password29',
    :password_confirmation => 'password29',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Sammy',
    :last_name => 'Salamanca',
    :description => "Ethereal Horizons” by Sammy Salamanca
The work focuses on abstract forms, landscapes and architectural elements, using  vibrant colors and engaging compositions.Horizon that hides behind events build cityscapes common in Salamanca ’s work. In this series he creates an ethereal world through his poetic imagination.
Color may be the most important design element  Salamanca  often uses color to provide an interesting design that attract viewers. The color allows water effects and sets that build apparent impossible worlds, different combinations where high contrasts predomínate.
Salamanca applies the paint with his own handmade tools, spatulas with different numbers of teeth and different sizes to generate displacement effects. He uses oils,acrylic and enamels. Recent paintings verge on the abstract, many contrasting man-made structures against a background of local city landscapes. His talent as an artist is now brought to a wider audience and achieving recognition with excellent composition, a mix of simplicity, elegance and banality.
His work has been widely exhibited in Colombia.",
    :email => 'ssalamancapintor@gmail.com',
    :password => 'password31',
    :password_confirmation => 'password31',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Juana',
    :last_name => 'Gaviria',
    :description => "Color Into Space
Latam geometric abstraction, which is one of the most idiosyncratic contributions made by We to contemporary  art, by emphasizing the three-dimensional character of the painting, Juana forces the  viewer to inspect it as if it were a sculpture,  constitute a visual and intellectual ascesis, a kind of meditation in movement, creating communication with the viewer. Unable to detect the complete system, the viewer nevertheless notices a strong visual force holding everything together
Juana’s  geometric abstractions engage the viewer to actively explore relations between the shaped canvas, its embedded planes and surrounding architecture. What is so astonishing and gratifying about (Juana’s) work is the way that colorand shape, each as important as the other, interact to create each painting.
Juana´s  paintings can be described as having a certain movement and three-dimensionality while retaining a reductive quality that is   general to geometric abstraction. The hard-edged forms  are bordered by bright bands of contrasting color of varying angle and width producing multiple fields of form and color that play against one another. The surprisingly bright color provide a rich contrast to the somber predominant blacks, grays and whites, and add weight and movement to the paintings.
The emphatic diagonal movement in many of her works relates a feeling of containment within the picture frame, and a further implication of spatial depth.
Juana Gaviria, designer started studying art at Universidad de los Andes in Bogotá however  received   a   bachelor   of   Fine   arts   and  Architecture from Rhode Island School of Design in 2000.  Now a residence at  Flora   artistic   training   school for contemporary art.",
    :email => 'juanagaviria@yahoo.com',
    :password => 'password32',
    :password_confirmation => 'password32',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Diana',
    :last_name => 'Piñeres',
    :description => "Born and raised in Cartagena, Colombia, where she developed a love and passion for painting through the natural splendor of her beloved tropical city . Trained by the famous maestro Miguel Burgos, who allowed her to evolve in the technique, she has produced   works that denote her passion for painting.
Determination, continuous study, imagination, and a desire to excel   enables Diana to paint as she does. Her paintings mirror the beauty, color, and tranquility in the hundreds of tropical  landscapes . With a strong composition to capture the real essence of the   variety and intensity of greens that occur in nature.
Diana Pineres is captivated by this treasure of flora. Understanding how an abstract painting can embody elements of figuration, and a figurative painting can also",
    :telephone => '+57 3205744357 ',
    :email => 'dianapineres.art@gmail.com',
    :password => 'password33',
    :password_confirmation => 'password33',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Cecilia',
    :last_name => 'Herrera',
    :description => "Cecilia was born in Cartagena, Colombia, studied at Jean Pierre Accault Institute and ceramics at the School of Fine Arts of San Miguel de Allende, Guanajuato, Mexico. A distinctive characteristic of her paintings is a tilted perspective, in which the vantage point is located high above the ground, common experience in the New Mexico landscape. This unexpected perspective invests the work with an eerily objective psychological edge for a realist painter who is focused on contrasts of both colour and tone found in nature.
Expressive style that results in works that often straddle the line between realism and abstraction as Cecilias´s energetic brush strokes bring movement and life to her landscapes, constructed from bright, energetic dabs of pure color she captures detail, color, texture. 
She attempts to produce paintings that in some way are tactile, sensitive and meaningful as best she can create realism that is not simply photographic.
She has exhibited at the Museum of Modern Art in Cartagena in 2008, which he called  Our essential nature , and more than 50 exhibitions in several countries including Canada, Spain, France, Mexico, Chile, Dominican Republic.",
    :telephone => ' +57 3156809029 ',
    :email => 'ceciherrera76@hotmail.com',
    :password => 'password34',
    :password_confirmation => 'password34',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

user = User.new(
    :first_name => 'Miguel',
    :last_name => 'Diaz Granados',
    :description => "Contemporary painter Diazgrandos
Working as an abstract expressionist painter, derived from a profound contemplative process that convey powerful sentiments, vibrant color and eloquent design resulting  in visual poetry which allows him to articulate intangible and psychological themes capturing a magnetic vibrancy through an illusion of light and richness.
Diazgranados´s color palette oil paintings with excellent movement, texture, shape shows the expressive potential of line and the light and dark opposition. He celebrates the awesome forces of the universe, saturated with emotion. The viewer must let the painting suggest what it might be, and be willing to accept those suggestions without fear of misinterpretation.
Diazgranados was born in Cartagena, Colombia, and seemed destined for a career in architecture with art as a strong interest. He studied art at “Escuela de Bellas Artes”, Cartagena and in the spirit of innovation that characterizes his work began to find a way to reconcile painting and sculpture.  In the process, Miguel has created stylish pieces that are straightforward in their approach to combining forms of abstraction that now engage him to marked a path an ethereal composition and most important contributions flatter shapes sort of float
Miguel has exhibited extensively in Colombia,United States and France",
    :telephone => '+57 3123700965 ',
    :email => 'migueldiazgranados@hotmail.com',
    :password => 'password34',
    :password_confirmation => 'password34',
    :type_user => '1'
  )
user.skip_confirmation!
user.save!

Subject.create(name: 'Abstract')
Subject.create(name: 'Aerial')
Subject.create(name: 'Aeroplane')
Subject.create(name: 'Airplane')
Subject.create(name: 'Animal')
Subject.create(name: 'Architecture')
Subject.create(name: 'Automobile')
Subject.create(name: 'Beach')
Subject.create(name: 'Bicycle')
Subject.create(name: 'Bike')
Subject.create(name: 'Boat')
Subject.create(name: 'Body')
Subject.create(name: 'Botanic')
Subject.create(name: 'Business')
Subject.create(name: 'Calligraphy')
Subject.create(name: 'Car')
Subject.create(name: 'Cartoon')
Subject.create(name: 'Cats')
Subject.create(name: 'Celebrity')
Subject.create(name: 'Children')
Subject.create(name: 'Cinemas')
Subject.create(name: 'Cities')
Subject.create(name: 'Classical mithology')
Subject.create(name: 'Comics')
Subject.create(name: 'Cows')
Subject.create(name: 'Cousine')
Subject.create(name: 'Culture')
Subject.create(name: 'Dogs')
Subject.create(name: 'Education')
Subject.create(name: 'Erotic')
Subject.create(name: 'Family')
Subject.create(name: 'Fantasy')
Subject.create(name: 'Fashion')
Subject.create(name: 'Fish')
Subject.create(name: 'Floral')
Subject.create(name: 'Food')
Subject.create(name: 'Food and drink')
Subject.create(name: 'Garden')
Subject.create(name: 'Geometric')
Subject.create(name: 'Graffiti')
Subject.create(name: 'Health and beauty')
Subject.create(name: 'Home')
Subject.create(name: 'Horse')
Subject.create(name: 'Humor')
Subject.create(name: 'Interiors')
Subject.create(name: 'Kids')
Subject.create(name: 'Kitchen')
Subject.create(name: 'Landscape')
Subject.create(name: 'Languague')
Subject.create(name: 'Light')
Subject.create(name: 'Love')
Subject.create(name: 'Men')
Subject.create(name: 'Mortality')
Subject.create(name: 'Motor')
Subject.create(name: 'Motorbike')
Subject.create(name: 'Motorcycle')
Subject.create(name: 'Music')
Subject.create(name: 'Nature')
Subject.create(name: 'Nude')
Subject.create(name: 'Outer space')
Subject.create(name: 'Patterns')
Subject.create(name: 'People')
Subject.create(name: 'Performing arts')
Subject.create(name: 'Places')
Subject.create(name: 'Political')
Subject.create(name: 'Politics')
Subject.create(name: 'Pop culture/Celebrity')
Subject.create(name: 'Popular culture')
Subject.create(name: 'Portrait')
Subject.create(name: 'Religion')
Subject.create(name: 'Religious')
Subject.create(name: 'Rural life')
Subject.create(name: 'Sail boat')
Subject.create(name: 'Science')
Subject.create(name: 'Science/Technology')
Subject.create(name: 'Seascape')
Subject.create(name: 'Seasons')
Subject.create(name: 'Ship')
Subject.create(name: 'Sport')
Subject.create(name: 'Sports')
Subject.create(name: 'Still Life')
Subject.create(name: 'Technology')
Subject.create(name: 'Time')
Subject.create(name: 'Train')
Subject.create(name: 'Transportation')
Subject.create(name: 'Travel')
Subject.create(name: 'Tree')
Subject.create(name: 'Typography')
Subject.create(name: 'Wall')
Subject.create(name: 'Water')
Subject.create(name: 'Woman')
Subject.create(name: 'World Cuture')
Subject.create(name: 'Yatch')