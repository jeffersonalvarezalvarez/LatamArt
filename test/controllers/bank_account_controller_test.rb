require 'test_helper'

class BankAccountControllerTest < ActionDispatch::IntegrationTest
  test "should get bank:string" do
    get bank_account_bank:string_url
    assert_response :success
  end

  test "should get number:string" do
    get bank_account_number:string_url
    assert_response :success
  end

  test "should get country:string" do
    get bank_account_country:string_url
    assert_response :success
  end

end
